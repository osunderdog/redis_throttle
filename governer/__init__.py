import asyncio
from dataclasses import dataclass, field
from typing import Optional
from uuid import uuid4
from redis.asyncio import Redis
from redis.client import Pipeline
import logging
import asyncio
from uuid import uuid4

class LimitExceeded(BaseException):
    pass

@dataclass()
class Governer:
    # https://redis.com/redis-best-practices/basic-rate-limiting/
    #
    # messages per second.  Number of messages allowable per measurement_seconds
    # for example,
    # message_limit=100.0 and measurement_seconds=5 equals 20 messages per second
    # resolution of measurement seconds may be a problem at very small
    # measurement seconds values.
    redis: Redis
    gname: str = field(default_factory=lambda: str(uuid4.hex()))
    message_limit: int = field(default=10)
    time_limit_s: int = field(default=1)


    def key_name(self, limited_name: str) -> str:
        return f"{self.gname}:{limited_name}"

    async def allow(self, subkey: Optional[str] = None) -> None:
        # if key value length is greater than message limit then reject
        # if key does not exist then set it and set expire.  add self to list
        # if key does exist then add self to list
        if await self.redis.exists(self.key_name(subkey)):
            logging.debug(f'exists: {self.key_name(subkey)}')
            count = await self.redis.llen(self.key_name(subkey))
            logging.debug(f'count {count}')
            if count >= self.message_limit:
                raise LimitExceeded()
            await self.redis.lpushx(self.key_name(subkey), 1)
        else:
            # breakpoint()
            logging.debug(f"first time add. {self.key_name(subkey)}")
            # set the expiration
            # p: Pipeline
            # async with self.redis.pipeline() as p:
            #     print(f'first time {self.key_name(subkey)}:{self.time_limit_s}')
            #
            #     await p.lpush(self.key_name(subkey), self.key_name(subkey))
            #     await p.expire(self.key_name(subkey), self.time_limit_s)
            await self.redis.lpush(self.key_name(subkey), 1)
            await self.redis.expire(self.key_name(subkey), self.time_limit_s)