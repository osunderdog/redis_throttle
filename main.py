
from redis.asyncio import Redis
import asyncio


async def amain():
    redis: Redis = await Redis(host='localhost', port=6379)
    await redis.set("foo", 'BAR')
    print(f"value: {await redis.get('foo')}")

if __name__ == "__main__":
    asyncio.run(amain())
