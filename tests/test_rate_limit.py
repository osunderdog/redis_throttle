import pytest
import pytest_asyncio
from redis.asyncio import Redis
import asyncio
from uuid import uuid4
import logging
from governer import Governer, LimitExceeded
# Tests are running simultaneously (or close to it)
# So have to use keys that won't clash.


@pytest_asyncio.fixture(scope='function')
async def redis_connection():
    redis: Redis = await Redis(host='localhost', port=6379)
    redis.flushdb
    yield redis
    redis.flushdb
    await redis.close()


@pytest.mark.asyncio
async def test_limit_simple_raised(redis_connection):
    """
    with a low message limit (10) and sending 100 messages in a burst,
    we should hit LimitExceeded exception
    """
    message_limit = 10
    gov = Governer(redis_connection, "RATE_LIMIT01", message_limit=message_limit, time_limit_s=1)
    logging.debug(f"Governer: {gov}")
    allowed=0
    message_burst_count = 100

    with pytest.raises(LimitExceeded):
        for i in range(message_burst_count):
            logging.debug(f'iteration: {i}')
            await gov.allow()
            allowed += 1
    assert allowed == message_limit

@pytest.mark.asyncio
async def test_limit_simple_minimal(redis_connection):
    """
    Should be well under the message limit so all should succeed.
    """
    message_limit = 1000
    message_burst_count = 100

    gov = Governer(redis_connection, "RATE_LIMIT01", message_limit=message_limit, time_limit_s=1)
    logging.debug(f"Governer: {gov}")
    for i in range(message_burst_count):
        logging.debug(f'iteration: {i}')
        await gov.allow()


@pytest.mark.asyncio
async def test_limit_throw_away(redis_connection: Redis):
    """
    When we reach the rate limit, count up the number of messages that are skipped due to failed allow.
    The expected number of messages skipped is message_burst_count - message_limit
    """
    message_limit = 10
    await redis_connection.flushdb(asynchronous=True)
    assert await redis_connection.llen('RATE_LIMIT01') == 0
    gov = Governer(redis_connection, "RATE_LIMIT01", message_limit=message_limit, time_limit_s=1)
    logging.debug(f"Governer: {gov}")
    skip_count = 0
    message_burst_count = 100
    for i in range(message_burst_count):
        try:
            logging.debug(f'iteration: {i}')
            await gov.allow()
        except LimitExceeded as le:
            skip_count += 1


    assert skip_count == message_burst_count-message_limit
