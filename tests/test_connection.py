import pytest
import pytest_asyncio
from redis.asyncio import Redis
import asyncio
from uuid import uuid4

# Tests are running simultaneously (or close to it)
# So have to use keys that won't clash.


@pytest_asyncio.fixture(scope='function')
async def redis_connection():
    redis: Redis = await Redis(host='localhost', port=6379)
    redis.flushdb
    yield redis
    redis.flushdb
    await redis.close()


@pytest.mark.asyncio
async def test_set(redis_connection):
    key = uuid4().hex
    value = 'bar'.encode('utf-8')
    await redis_connection.set(key, value)
    actual_data = await redis_connection.get(key)
    assert value == actual_data


@pytest.mark.asyncio
async def test_list(redis_connection: Redis):
    list_key = uuid4().hex
    expected_values = [str(v).encode('utf-8') for v in [6, 77, 4, 22, 98, 10]]
    assert await redis_connection.llen(list_key) == 0

    await redis_connection.lpush(list_key, *expected_values)

    actual_data = await redis_connection.rpop(name=list_key, count=len(expected_values))
    assert actual_data == expected_values


@pytest.mark.asyncio
async def test_list_expire(redis_connection: Redis):
    list_key = uuid4().hex
    expected_values = [str(v).encode('utf-8') for v in [6, 77, 4, 22, 98, 10]]
    assert await redis_connection.llen(list_key) == 0

    await redis_connection.lpush(list_key, *expected_values)
    await redis_connection.expire(list_key, time=1)
    assert await redis_connection.exists(list_key)
    await asyncio.sleep(1.0)
    assert not await redis_connection.exists(list_key)

